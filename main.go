// Rdrovins reads the data stream from the iXBlue Rovins INS devices and
// publishes the data records to a NATS Streaming Server.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/ixblue"
	"github.com/nats-io/stan.go"
)

const Usage = `Usage: rdrovins [options] host:port

Read data the iXBlue Rovins INS at HOST:PORT and publish the data records to
a NATS Streaming Server.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL       string        = "nats://localhost:4222"
	clusterID     string        = "must-cluster"
	rovinsSubject string        = "rovins.data"
	rdTimeout     time.Duration = time.Second * 5
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&rovinsSubject, "sub", lookupEnvOrString("ROVINS_SUBJECT", rovinsSubject),
		"Subject name for published data")
	flag.DurationVar(&rdTimeout, "timeout",
		lookupEnvOrDuration("ROVINS_TIMEOUT", rdTimeout),
		"read timeout")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	status := 0
	defer func() { os.Exit(status) }()

	sc, err := stan.Connect(clusterID, "rovins-pub", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect to NATS: %v", err)
	}
	defer sc.Close()

	conn, err := net.Dial("tcp", args[0])
	if err != nil {
		log.Printf("Cannot access Rovins at %s: %v", args[0], err)
		status = 1
		return
	}
	defer conn.Close()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("Rovins publisher starting %s", Version)

	dec := ixblue.NewDecoder(conn)
	for {
		select {
		case <-sigs:
			return
		default:
		}
		conn.SetReadDeadline(time.Now().Add(rdTimeout))
		rec, err := dec.Decode()
		if err != nil {
			log.Printf("Rovins record error: %v", err)
			status = 1
			return
		}
		b, _ := rec.MarshalBinary()
		sc.Publish(rovinsSubject, b)
	}
}
