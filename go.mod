module bitbucket.org/uwaploe/rdrovins

go 1.15

require (
	bitbucket.org/uwaploe/ixblue v0.2.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.2.0 // indirect
	github.com/nats-io/nats-streaming-server v0.21.1 // indirect
	github.com/nats-io/stan.go v0.8.3
)
